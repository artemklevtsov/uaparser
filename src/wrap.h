#include <RcppCommon.h>
#include <UaParser.h>

namespace Rcpp {

template <> SEXP wrap(const std::vector<uap_cpp::Device>&);
template <> SEXP wrap(const std::vector<uap_cpp::Agent>&);
template <> SEXP wrap(const std::vector<uap_cpp::UserAgent>&);
template <> SEXP wrap(const std::vector<uap_cpp::DeviceType>&);

} // Rcpp

#include <Rcpp.h>
