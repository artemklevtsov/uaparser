#include "wrap.h"

using namespace uap_cpp;

inline Rcpp::String coalesce_string(const std::string& x) {
  return x.empty() ? NA_STRING : Rcpp::String(x);
}

inline Rcpp::String device_type_to_string(const DeviceType& x) {
  switch(x) {
  case DeviceType::kDesktop:
    return "desktop";
  case DeviceType::kMobile:
    return "mobile";
  case DeviceType::kTablet:
    return "tablet";
  case DeviceType::kUnknown:
    return "unknown";
  default:
    return "unknown";
  }
}

namespace Rcpp {

template<>
SEXP wrap(const std::vector<DeviceType>& x) {
  size_t n = x.size();
  StringVector res = no_init(n);
  for (size_t i = 0; i < n; ++i) {
    res[i] = device_type_to_string(x[i]);
  }
  return res;
}

template<>
SEXP wrap(const std::vector<Device>& x) {
  size_t n = x.size();
  
  StringVector family = no_init(n);
  StringVector brand = no_init(n);
  StringVector model = no_init(n);
  
  for (size_t i = 0; i < n; ++i) {
    family[i] = coalesce_string(x[i].family);
    brand[i] = coalesce_string(x[i].brand);
    model[i] = coalesce_string(x[i].model);
  }
  
  DataFrame res = DataFrame::create(
    Named("family") = family,
    Named("brand") = brand,
    Named("model") = model,
    Named("stringsAsFactors") = false
  );
  return res;
}

template<>
SEXP wrap(const std::vector<Agent>& x) {
  size_t n = x.size();
  
  StringVector family = no_init(n);
  StringVector ver_major = no_init(n);
  StringVector ver_minor = no_init(n);
  StringVector ver_patch = no_init(n);
  StringVector ver_patch_minor = no_init(n);
  
  for (size_t i = 0; i < n; ++i) {
    family[i] = coalesce_string(x[i].family);
    ver_major[i] = coalesce_string(x[i].major);
    ver_minor[i] = coalesce_string(x[i].minor);
    ver_patch[i] =  coalesce_string(x[i].patch);
    ver_patch_minor[i] = coalesce_string(x[i].patch_minor);
  }
  
  DataFrame res = DataFrame::create(
    Named("family") = family,
    Named("ver_major") = ver_major,
    Named("ver_minor") = ver_minor,
    Named("ver_patch") = ver_patch,
    Named("ver_patch_minor") = ver_patch_minor,
    Named("stringsAsFactors") = false
  );
  return res;
}

template<>
SEXP wrap(const std::vector<UserAgent>& x) {
  size_t n = x.size();
  
  StringVector os_family = no_init(n);
  StringVector os_ver_major = no_init(n);
  StringVector os_ver_minor = no_init(n);
  StringVector os_ver_patch = no_init(n);
  StringVector os_ver_patch_minor = no_init(n);
  StringVector device_family = no_init(n);
  StringVector device_model = no_init(n);
  StringVector device_brand = no_init(n);
  StringVector browser_family = no_init(n);
  StringVector browser_ver_major = no_init(n);
  StringVector browser_ver_minor = no_init(n);
  StringVector browser_ver_patch = no_init(n);
  StringVector browser_ver_patch_minor = no_init(n);
  
  for (size_t i = 0; i < n; ++i) {
    os_family[i] = coalesce_string(x[i].os.family);
    os_ver_major[i] = coalesce_string(x[i].os.major);
    os_ver_minor[i] = coalesce_string(x[i].os.minor);
    os_ver_patch[i] =  coalesce_string(x[i].os.patch);
    os_ver_patch_minor[i] = coalesce_string(x[i].os.patch_minor);
    device_family[i] = coalesce_string(x[i].device.family);
    device_model[i] =  coalesce_string(x[i].device.model);
    device_brand[i] = coalesce_string(x[i].device.brand);
    browser_family[i] = coalesce_string(x[i].browser.family);
    browser_ver_major[i] = coalesce_string(x[i].browser.major);
    browser_ver_minor[i] = coalesce_string(x[i].browser.minor);
    browser_ver_patch[i] =  coalesce_string(x[i].browser.patch);
    browser_ver_patch_minor[i] = coalesce_string(x[i].browser.patch_minor);
  }
  
  DataFrame res = DataFrame::create(
    Named("os_family") = os_family,
    Named("os_ver_major") = os_ver_major,
    Named("os_ver_minor") = os_ver_minor,
    Named("os_ver_patch") = os_ver_patch,
    Named("os_ver_patch_minor") = os_ver_patch_minor,
    Named("device_family") = device_family,
    Named("device_model") = device_model,
    Named("device_brand") = device_brand,
    Named("browser_family") = browser_family,
    Named("browser_ver_major") = browser_ver_major,
    Named("browser_ver_minor") = browser_ver_minor,
    Named("browser_ver_patch") = browser_ver_patch,
    Named("browser_ver_patch_minor") = browser_ver_patch_minor,
    Named("stringsAsFactors") = false
  );
  
  return res;
}
}
