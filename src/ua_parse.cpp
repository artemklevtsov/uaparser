#ifdef _OPENMP
# include <omp.h>
#endif
#include <UaParser.h>
#include <Rcpp.h>
#include "wrap.h"

using namespace Rcpp;
using namespace uap_cpp;

static UserAgentParser* uap = nullptr;

// [[Rcpp::export]]
void uap_init(const std::string& yaml_file) {
  if (uap) {
    delete uap;
  }
  uap = new UserAgentParser(yaml_file);
}

// [[Rcpp::export]]
void uap_release() {
    if (uap) {
        delete uap;
        uap = nullptr;
    }
}

//' @title Parse User Agents
//' @description
//' Take a vector of user agents and parse them into their component parts.
//'
//' @param x A character vector of user agents.
//'
//' @return A data.frame consisting of those fields you wanted to return.
//'
//' @export
//'
//' @examples
//' # user agent strings
//' ua <- c(
//'   "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0",
//'   "Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0",
//'   "Mozilla/5.0 (compatible; MSIE 9.0; Windows Phone OS 7.5; Trident/5.0; IEMobile/9.0)",
//'   "curl/7.10.6 (i386-redhat-linux-gnu) libcurl/7.10.6 OpenSSL/0.9.7a ipv6 zlib/1.1.4",
//'   "Googlebot/2.1 (+http://www.google.com/bot.html)"
//' )
//' # parse all fields
//' parse_user_agent(ua)
//' # parse os information
//' parse_os(ua)
//' # get device information
//' parse_device(ua)
//' # parse browser information
//' parse_browser(ua)
//' # get device type
//' parse_device_type(ua)
//'
// [[Rcpp::export]]
SEXP parse_user_agent(StringVector x) {
  size_t n = x.size();
  std::vector<UserAgent> res(n);
  #pragma omp parallel for
  for (size_t i = 0; i < n; ++i) {
    std::string tmp = as<std::string>(x[i]);
    res[i] = uap->parse(tmp);
  }
  return wrap(res);
}

//' @rdname parse_user_agent
//' @export
// [[Rcpp::export]]
SEXP parse_os(StringVector x) {
  size_t n = x.size();
  std::vector<Agent> res(n);
  #pragma omp parallel for
  for (size_t i = 0; i < n; ++i) {
    std::string tmp = as<std::string>(x[i]);
    res[i] = uap->parse_os(tmp);
  }
  return wrap(res);
}

//' @rdname parse_user_agent
//' @export
// [[Rcpp::export]]
SEXP parse_device(StringVector x) {
  size_t n = x.size();
  std::vector<Device> res(n);
  #pragma omp parallel for
  for (size_t i = 0; i < n; ++i) {
    std::string tmp = as<std::string>(x[i]);
    res[i] = uap->parse_device(tmp);
  }
  return wrap(res);
}

//' @rdname parse_user_agent
//' @export
// [[Rcpp::export]]
SEXP parse_browser(StringVector x) {
  size_t n = x.size();
  std::vector<Agent> res(n);
  #pragma omp parallel for
  for (size_t i = 0; i < n; ++i) {
    std::string tmp = as<std::string>(x[i]);
    res[i] = uap->parse_browser(tmp);
  }
  return wrap(res);
}

//' @rdname parse_user_agent
//' @export
// [[Rcpp::export]]
SEXP parse_device_type(StringVector x) {
  size_t n = x.size();
  std::vector<DeviceType> res(n);
  #pragma omp parallel for
  for (size_t i = 0; i < n; ++i) {
    std::string tmp = as<std::string>(x[i]);
    res[i] = uap->device_type(tmp);
  }
  return wrap(res);
}
