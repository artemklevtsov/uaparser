
<!-- README.md is generated from README.Rmd. Please edit that file -->

# uaparser

<!-- badges: start -->

[![GitLab CI Build
Status](https://gitlab.com/artemklevtsov/uaparser/badges/master/pipeline.svg)](https://gitlab.com/artemklevtsov/uaparser/pipelines)
[![AppVeyor Build
Status](https://ci.appveyor.com/api/projects/status/p8q92u872u9f53lq?svg=true)](https://ci.appveyor.com/project/artemklevtsov/uaparser)
[![Codecov Code
Coverage](https://codecov.io/gl/artemklevtsov/uaparser/branch/master/graph/badge.svg)](https://codecov.io/gl/artemklevtsov/uaparser)
[![CRAN
Status](http://www.r-pkg.org/badges/version/uaparser)](https://cran.r-project.org/package=uaparser)
[![License: GPL
v2](https://img.shields.io/badge/License-GPL%20v2-blue.svg)](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)

<!-- badges: end -->

R bindings for [`uap-cpp`](https://github.com/ua-parser/uap-cpp)
library, that is user agent parser.

## Installation

To install the package from the CRAN run the following command:

``` r
install.packages("uaparser", repos = "https://cloud.r-project.org/")
```

Also you could install the dev-version with the `install_gitlab()`
function from the `remotes` package:

``` r
remotes::install_gitlab("artemklevtsov/uaparser@devel")
```

This package contains the compiled code, therefore you have to use the
[Rtools](https://cran.r-project.org/bin/windows/Rtools/) to install it
on Windows.

## Example

``` r
# load packages
library(uaparser)
# user agent strings
ua <- c(
  "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0",
  "Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0",
  "Mozilla/5.0 (compatible; MSIE 9.0; Windows Phone OS 7.5; Trident/5.0; IEMobile/9.0)",
  "curl/7.10.6 (i386-redhat-linux-gnu) libcurl/7.10.6 OpenSSL/0.9.7a ipv6 zlib/1.1.4",
  "Googlebot/2.1 (+http://www.google.com/bot.html)"
)
# parse all fields
parse_user_agent(ua)
#>       os_family os_ver_major os_ver_minor os_ver_patch os_ver_patch_minor      device_family
#> 1       Windows            7         <NA>         <NA>               <NA>              Other
#> 2      Mac OS X         <NA>         <NA>         <NA>               <NA>              Other
#> 3 Windows Phone            7            5         <NA>               <NA> Generic Smartphone
#> 4         Linux         <NA>         <NA>         <NA>               <NA>              Other
#> 5         Other         <NA>         <NA>         <NA>               <NA>             Spider
#>   device_model device_brand browser_family browser_ver_major browser_ver_minor browser_ver_patch
#> 1         <NA>         <NA>        Firefox                47                 0              <NA>
#> 2         <NA>         <NA>        Firefox                42                 0              <NA>
#> 3   Smartphone      Generic      IE Mobile                 9                 0              <NA>
#> 4         <NA>         <NA>           curl                 7                10                 6
#> 5      Desktop       Spider      Googlebot                 2                 1              <NA>
#>   browser_ver_patch_minor
#> 1                    <NA>
#> 2                    <NA>
#> 3                    <NA>
#> 4                    <NA>
#> 5                    <NA>
# parse os information
parse_os(ua)
#>          family ver_major ver_minor ver_patch ver_patch_minor
#> 1       Windows         7      <NA>      <NA>            <NA>
#> 2      Mac OS X      <NA>      <NA>      <NA>            <NA>
#> 3 Windows Phone         7         5      <NA>            <NA>
#> 4         Linux      <NA>      <NA>      <NA>            <NA>
#> 5         Other      <NA>      <NA>      <NA>            <NA>
# get device information
parse_device(ua)
#>               family   brand      model
#> 1              Other    <NA>       <NA>
#> 2              Other    <NA>       <NA>
#> 3 Generic Smartphone Generic Smartphone
#> 4              Other    <NA>       <NA>
#> 5             Spider  Spider    Desktop
# parse browser information
parse_browser(ua)
#>      family ver_major ver_minor ver_patch ver_patch_minor
#> 1   Firefox        47         0      <NA>            <NA>
#> 2   Firefox        42         0      <NA>            <NA>
#> 3 IE Mobile         9         0      <NA>            <NA>
#> 4      curl         7        10         6            <NA>
#> 5 Googlebot         2         1      <NA>            <NA>
# get device type
parse_device_type(ua)
#> [1] "desktop" "desktop" "mobile"  "desktop" "desktop"
```

## Bug reports

Use the following command to go to the page for bug report submissions:

``` r
bug.report(package = "uaparser")
```

Before reporting a bug or submitting an issue, please do the following:

  - Make sure that you error or issue was not reported or discussed
    earlier. Please, use the search;
  - Check the news list of the current version. Some errors could be
    caused by the package changes. It could be done with `news(package =
    "uaparser", Version == packageVersion("uaparser"))` command;
  - Make a minimal reproducible example of the code that consistently
    causes the error;
  - Make sure that the error occurs during the execution of a function
    from the `uaparser` package, not from other packages;
  - Try to reproduce the error with the last development version of the
    package from the git repository.

Please attach traceback() and sessionInfo() output to bug report. It may
save a lot of time.

## License

The `uaparser` package is distributed under
[GPLv2](http://www.gnu.org/licenses/gpl-2.0.html) license.
